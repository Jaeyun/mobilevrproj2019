﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRProject.Interface.Camera;

namespace VRProject.Custom.Interface.System
{
    public class EditorManager : MonoBehaviour
    {
        [SerializeField] private PlayerCameraController m_playerCameraController;
        [SerializeField] private UICameraController m_uiCameraController;

#if UNITY_EDITOR
        public void Update()
        {
            if(Input.GetKeyDown(KeyCode.V))
            {
                m_playerCameraController.ChangeCameraModeByValue(Domain.Camera.CameraMode.Virtual);
                m_uiCameraController.ChangeCameraModeByValue(Domain.Camera.CameraMode.Virtual);
            }
            if(Input.GetKeyDown(KeyCode.N))
            {
                m_playerCameraController.ChangeCameraModeByValue(Domain.Camera.CameraMode.Normal);
                m_uiCameraController.ChangeCameraModeByValue(Domain.Camera.CameraMode.Normal);
            }
        }
#endif
    }
}