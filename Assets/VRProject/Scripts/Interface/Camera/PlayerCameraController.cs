﻿using UnityEngine;
using VRProject.Domain.Camera;
using UniRx.Async;

namespace VRProject.Interface.Camera
{
    /// <summary>
    /// Playerのカメラの切り替えを管理する
    /// </summary>
    /// @Choi 2019.06.23
    public class PlayerCameraController : MonoBehaviour
    {
        /// <summary>
        /// NormalカメラObject
        /// </summary>
        [SerializeField] private NormalCamera m_normalCamera;

        /// <summary>
        /// VirtualモードカメラObject
        /// </summary>
        [SerializeField] private VirtualCamera m_virtualCamera;

        private void Awake()
        {
            if (m_normalCamera == null)
            {

            }
        }

        public bool Init()
        {
            if (!m_normalCamera.Active()
                || m_virtualCamera.Inactive())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Changes the camera mode.
        /// </summary>
        /// <param name="mode">Mode.</param>
        public bool ChangeCameraModeByValue(CameraMode mode)
        {
            CameraValueObject.Instance.SetValue(mode);
            if (mode == CameraMode.Virtual)
            {
                if (!m_normalCamera.Inactive()
                    || !m_virtualCamera.Active())
                {
                    return false;
                }
                return true;
            }
            else
            {
                if (!m_normalCamera.Active()
                    || !m_virtualCamera.Inactive())
                {
                    return false;
                }
                return true;
            }
        }
    }
}