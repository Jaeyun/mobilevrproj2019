﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRProject.Domain.Camera;

namespace VRProject.Interface.Camera
{
    /// <summary>
    /// VR UICamera Objectに紐づけるコンポーネント
    /// </summary>
    public class VirtualUICamera : MonoBehaviour, ICamera
    {
        /// <summary>
        /// Active Camera
        /// </summary>
        /// <returns>The active Result.</returns>
        public bool Active()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
            return true;
        }

        /// <summary>
        /// Inactive Camera
        /// </summary>
        /// <returns>The inactive Result.</returns>
        public bool Inactive()
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
            return true;
        }
    }
}