﻿using UnityEngine;
using VRProject.Domain.Camera;
using UniRx.Async;

namespace VRProject.Interface.Camera
{
    /// <summary>
    /// Playerのカメラの切り替えを管理する
    /// </summary>
    /// @Choi 2019.06.23
    public class UICameraController : MonoBehaviour
    {
        /// <summary>
        /// NormalカメラUIObject
        /// </summary>
        [SerializeField] private NormalUICamera m_normalUICamera;

        /// <summary>
        /// VirtualモードUIカメラObject
        /// </summary>
        [SerializeField] private VirtualUICamera m_virtualUICamera;

        private void Awake()
        {
        }

        public bool Init()
        {
            if (!m_normalUICamera.Active()
                ||!m_virtualUICamera.Inactive())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Changes the camera mode.
        /// </summary>
        /// <param name="mode">Mode.</param>
        public bool ChangeCameraModeByValue(CameraMode mode)
        {
            CameraValueObject.Instance.SetValue(mode);
            if (mode == CameraMode.Virtual)
            {
                if (!m_normalUICamera.Inactive()
                    || !m_virtualUICamera.Active())
                {
                    return false;
                }
                return true;
            }
            else
            {
                if (!m_normalUICamera.Active()
                    || !m_virtualUICamera.Inactive())
                {
                    return false;
                }
                return true;
            }
        }
    }
}