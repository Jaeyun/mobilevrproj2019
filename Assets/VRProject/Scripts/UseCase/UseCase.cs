﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

namespace VRProject.UseCase.Camera
{
    /// <summary>
    /// UseCase Base
    /// </summary>
    /// @Choi 2019.07.06
    public abstract class UseCase
    {
        /// <summary>
        /// 実行
        /// </summary>
        /// <returns>結果</returns>
        public bool Execute()
        {
            return ExecuteImpl();
        }

        /// <summary>
        /// 実際の具象メソッド
        /// </summary>
        /// <returns><c>true</c>, if impl was executed, <c>false</c> otherwise.</returns>
        protected abstract bool ExecuteImpl();
    }
}