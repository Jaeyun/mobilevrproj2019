﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRProject.UseCase.Camera
{
    /// <summary>
    /// Camera change process UseCase.
    /// </summary>
    public class CameraChangeProcUseCase : UseCase
    {
        protected override bool ExecuteImpl()
        {
            return true;
        }
    }
}