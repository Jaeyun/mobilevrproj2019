﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRProject.Domain.Camera
{
    public enum CameraMode
    {
        NONE,
        Normal,
        Virtual,
    }
}