﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VRProject.Domain.Camera
{
    public class CameraValueObject
    {
        private static Lazy<CameraValueObject> m_instance = new Lazy<CameraValueObject>(() => new CameraValueObject());

        public static CameraValueObject Instance { get { return m_instance.Value; } }

        public CameraMode Value { get; private set; }

        public void SetValue(CameraMode mode)
        {
            Value = mode;
        }
    }
}