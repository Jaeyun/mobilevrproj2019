﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRProject.Domain.Camera
{
    /// <summary>
    /// Camera制御の共通規約Interface
    /// </summary>
    public interface ICamera
    {
        /// <summary>
        /// Active Camera
        /// </summary>
        /// <returns>The active Result.</returns>
        bool Active();

        /// <summary>
        /// Inactive Camera
        /// </summary>
        /// <returns>The inactive Result.</returns>
        bool Inactive();
    }
}
